﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLabForWorkShop
{
   public class RightAngledTriangle:Triangle
    {
        public int hypotenuse { get; set; }

        public RightAngledTriangle(int l, int h, int hypo)
        {
            length = l;
            height = h;
            hypotenuse = hypo;
        }
        //override the functions her

        //override the functions her
        public override void DisplayDimensions()
        {
            Console.WriteLine("length =" + length + " height =" + height);
        }
        public override void DisplayArea()
        {
            Console.WriteLine("Area = " + (0.5 * length * height));
        }
        public override void DisplayPerimeter()
        {
            Console.WriteLine("Perimeter = " + (0.5 * length * height));
        }
    }
}
