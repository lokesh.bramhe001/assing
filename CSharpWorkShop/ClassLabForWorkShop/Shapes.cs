﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLabForWorkShop
{
    public  abstract class Shapes
    {
        
        public string shape { get; set; }
        public abstract void DisplayDimensions();
        public abstract void DisplayArea();
        public abstract void DisplayPerimeter();
        public  virtual void DisplayType()
        {

        }
    }
}
