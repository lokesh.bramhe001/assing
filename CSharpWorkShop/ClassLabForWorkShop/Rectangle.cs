﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLabForWorkShop
{
    public class Rectangle : Shapes
    {
       
            public int length { get; set; }
        public int breadth { get; set; }
        public Rectangle(int l, int b)
        {
            length = l;
            breadth = b;
        }
        public override void DisplayArea()
        {
            Console.WriteLine("Area = "+(length*breadth));
            //throw new NotImplementedException();
        }

        public override void DisplayDimensions()
        {
            Console.WriteLine("Length = "+length+"breadth ="+breadth);
            //throw new NotImplementedException();
        }

        public override void DisplayPerimeter()
        {
            Console.WriteLine("Perimeter ="+((2*length)+(2*breadth)));
            //throw new NotImplementedException();
        }
    }
}
