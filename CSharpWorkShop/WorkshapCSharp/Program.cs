﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLabForWorkShop;
namespace WorkshapCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Shapes s1 = new Rectangle(10, 10);
            Shapes s2 = new RightAngledTriangle(10, 10,10);
            Shapes s3 = new Rectangle(20, 20);

            ArrayList shapess = new ArrayList();
            shapess.Add(s1);
            shapess.Add(s2);
            shapess.Add(s3);
            foreach (Shapes item in shapess)
            {
                item.DisplayArea();
                Console.WriteLine();
                item.DisplayPerimeter();
                Console.WriteLine();
                item.DisplayDimensions();
            }
            Console.WriteLine("------");
            Stack st = new Stack();
            st.Push(s1);
            st.Push(s2);
            st.Push(s3);
           /* while (st.Count>0)
            {*/
                foreach (Shapes item in st)
                {
                    item.DisplayPerimeter();
                   
                }
          //  }
           /* while (st.Count>0)
            {*/
                st.Pop();
                st.Pop();
                st.Pop();
            // }
            Console.ReadLine();
        }
    }
}
