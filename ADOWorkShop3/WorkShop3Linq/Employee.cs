﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkShop3Linq
{
    //DeptID->int, Name->string, Doj->datetime,Sal->double, Designation->strin
    public class Employee
    {
        public int DeptID { get; set; }
        public string Name { get; set; }
        public DateTime Doj { get; set; }
        public double Sal { get; set; }
        public string Designation { get; set; }
    }
}
