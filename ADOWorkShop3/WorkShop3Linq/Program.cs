﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkShop3Linq
{
    class Program
    {//DeptID->int, Name->string, Doj->datetime,Sal->double, Designation->strin
        static void Main(string[] args)
        {
            Employee[] employees =
            {
                new Employee{DeptID= 10,Name=" Harshad",Doj=new DateTime(2014,06,30),
                    Sal=65000.00,Designation="System Analyst" },
                new Employee{DeptID= 12,Name=" Anagha",Doj=new DateTime(2018,03,14),
                    Sal=89500.00,Designation="Manager" },
                new Employee{DeptID= 5,Name=" Swizzle",Doj=new DateTime(2019,06,20),
                    Sal=78500.00,Designation="Team Lead" },

                new Employee{DeptID= 17,Name=" Brotin",Doj=new DateTime(2012,07,06),
                    Sal=65421.00,Designation="Testing Engineer" },
                
                new Employee{DeptID= 17,Name=" Pravin",Doj=new DateTime(2010,08,10),
                    Sal=78605.50,Designation="System Analyst" },
               
                new Employee{DeptID= 10,Name=" Anant",Doj=new DateTime(2013,09,04),
                    Sal=56000.00,Designation="Quality Analyst" },
                
                new Employee{DeptID= 5,Name=" Viraj",Doj=new DateTime(2019,10,03),
                    Sal=37500.50,Designation="Developer" },
                
                new Employee{DeptID= 3,Name=" Namrata",Doj=new DateTime(2020,02,14),
                    Sal=42500.50,Designation="System Analyst" },
               
                new Employee{DeptID= 17,Name=" Mitali",Doj=new DateTime(2014,01,23),
                    Sal=62500.50,Designation="Team Lead" },
                  
                new Employee{DeptID= 10,Name=" Swayam",Doj=new DateTime(2012,10,13),
                    Sal=27500.50,Designation="Testing Engineer" },
                
                new Employee{DeptID= 2,Name=" Zeeshan",Doj=new DateTime(2016,08,18),
                    Sal=22500.50,Designation="Developer" },
            };
            // Display all the employees whose SAL is less than 70000 
            Console.WriteLine("All Employees");
            foreach (Employee item in employees)
            {
                Console.WriteLine(item.DeptID+" "+item.Name+" "+item.Doj+" "+item.Sal+" "+
                    item.Designation);
            }

            // Display all the employees whose SAL is less than 70000 
            Console.WriteLine("**************************************************");
            Console.WriteLine("all the employees whose SAL is less than 70000 ");
            var employeesWhoseSALIsLessThan70000 = (from employeeObj in employees
                                                    where employeeObj.Sal<70000
                                                    select employeeObj).ToArray();
            foreach (Employee item in employeesWhoseSALIsLessThan70000)
            {
                Console.WriteLine(item.DeptID + " " + item.Name + " " + item.Doj + " " + 
                    item.Sal + " " + item.Designation);
            }
            //Display all the employees who are working as MANAGER or ANALYST
            Console.WriteLine("**************************************************");
            Console.WriteLine("all the employees who are working as MANAGER or ANALYST ");
            var employeesWhoAreWorkingAsMANAGERoANALYST = (from employeeObj in employees
                                                    where employeeObj.Designation='MANAGER'
                                                           select employeeObj).ToArray();
            foreach (Employee item in employeesWhoseSALIsLessThan70000)
            {
                Console.WriteLine(item.DeptID + " " + item.Name + " " + item.Doj + " " +
                    item.Sal + " " + item.Designation);
            }
            Console.ReadLine();
        }
    }
}
