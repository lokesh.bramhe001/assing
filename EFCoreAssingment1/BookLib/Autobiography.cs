﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLib
{
   public  class Autobiography
    {
        [Key]
        public int AuthorBiographyId { get; set; }
        public string BiographyTitle { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Nationality { get; set; }

        public int AuthorId { get; set; }

    }
}
