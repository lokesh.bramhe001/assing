﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookLib
{
    public class Author
    {
        [Key]
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int BookId { get; set; }
        public List<Book> Books { get; set; }

        public int AutobiographyId { get; set; }
    }
}
