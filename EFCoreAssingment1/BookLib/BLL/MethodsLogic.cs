﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLib.BLL
{
   public  class MethodsLogic
    {
        public void addBook()
        {
            Console.WriteLine("Enter The Book Title");
            string title = Console.ReadLine();
            Console.WriteLine("Enter The Book Category Id");
            Console.WriteLine("1 : Motivational"); 
            Console.WriteLine("2 : Sport"); 
            Console.WriteLine("3 : Crime"); 
            Console.WriteLine("4 : Cybage"); 
            Console.WriteLine("5 : DotNet");
            int  Categid =Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter The Book Title");
            string title = Console.ReadLine();
            using (var book = new BooksDBContext())
            {
                Book bookObj = new Book()
                {
                    Title = title,

                };
                book.Books.Add(bookObj);
                book.SaveChanges();
            }
        }
        public void addCategory()
        {
            Console.WriteLine("Enter The Book Category");
            string category = Console.ReadLine();
            using (var cate = new BooksDBContext())
            {
                Category categoryObj = new Category()
                {
                    CategoryName = category
                };
                cate.Categories.Add(categoryObj);
                cate.SaveChanges();
            }
        }
        public void addAuthor()
        {
            Console.WriteLine("Enter The Author First Name");
            string fName = Console.ReadLine();
            Console.WriteLine("Enter The Author Last Name");
            string lName = Console.ReadLine();
            using (var auth = new BooksDBContext())
            {
                Author authorObj = new Author()
                {
                    FirstName = fName,
                    LastName=lName
                };
                auth.Authors.Add(authorObj);
                auth.SaveChanges();
            }
        }
    }
}
