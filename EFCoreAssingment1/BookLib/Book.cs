﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLib
{
   public  class Book
    {
        [Key]
        public int BookId { get; set; }
        public string Title { get; set; }

        public int CategoryId { get; set; }

        public int AuthorId { get; set; }
        public List<Author> Authors { get; set; }
    }
}
